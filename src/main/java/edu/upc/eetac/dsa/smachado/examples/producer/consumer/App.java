package edu.upc.eetac.dsa.smachado.examples.producer.consumer;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ChumBucket c = new ChumBucket();
		Producer p1 = new Producer(c, 1);
		Consumer c1 = new Consumer(c, 1);
		p1.start();
		c1.start();
	}
}
