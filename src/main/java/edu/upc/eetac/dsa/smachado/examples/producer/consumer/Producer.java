package edu.upc.eetac.dsa.smachado.examples.producer.consumer;

public class Producer extends Thread {
	private ChumBucket chumBucket;
	private int number;

	public Producer(ChumBucket c, int number) {
		chumBucket = c;
		this.number = number;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			chumBucket.put(i);
			System.out.println("Producer #" + this.number + " put: " + i);
			try {
				sleep((int) (Math.random() * 100));
			} catch (InterruptedException e) {
			}
		}
	}
}
