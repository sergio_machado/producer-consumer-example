package edu.upc.eetac.dsa.smachado.examples.producer.consumer;

public class Consumer extends Thread {
	private ChumBucket chumBucket;
	private int number;

	public Consumer(ChumBucket c, int number) {
		chumBucket = c;
		this.number = number;
	}

	public void run() {
		int value = 0;
		for (int i = 0; i < 10; i++) {
			value = chumBucket.get();
			System.out.println("Consumer #" + this.number + " got: " + value);
		}
	}
}
